﻿using System;

namespace Task11_PaymentSystem
{
    class Program
    {
        static void Main(string[] args)
        {

            //MAKE SOME INSTANCES TO SHOW THE RESULT
            CreditCard myCredit = new CreditCard();
            myCredit.Balance = 4000;
            myCredit.Deposit(1000);

            SavingCard mySavingCard = new SavingCard();
            mySavingCard.Balance = 5000;
            mySavingCard.Withdraw(1000);

            Coin myCoin = new Coin(5);
            myCoin.ToString();

            Note myNote = new Note(100);
            myNote.ToString();

            

        }
    }
}
