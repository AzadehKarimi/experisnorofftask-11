﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class SavingCard:Card
    {
        //USING PARENT(CARD) METHODES
        public override double Withdraw(double withdrawAmount)
        {
            if (withdrawAmount<=Balance)
            {
                Console.WriteLine($"Your balance in saving card is  {Balance = Balance - withdrawAmount}");

                return Balance;
            }
            else
            {
                Console.WriteLine("insufficiant found in saving card ");
            }
            return Balance;
            
            
        }
        public override double Deposit(double depositAmount)
        {
            Console.WriteLine($"Your balance in saving card  is  { Balance = Balance + depositAmount}");
            return Balance ;
            
        }
    }
}
