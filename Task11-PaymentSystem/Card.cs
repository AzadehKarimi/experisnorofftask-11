﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    abstract class Card
    {
        private string cardNumber;
        private double balance;
        private string accountNumber;
        //CONSTRUCTOR
        public Card()
        {

        }
        //CONSTRUCTOR
        public Card(string cardNumber, double balance, string accountNumber)
        {
            this.CardNumber = cardNumber;
            this.Balance = balance;
            this.AccountNumber = accountNumber;
        }



        public string CardNumber { get => cardNumber; set => cardNumber = value; }
        public double Balance { get => balance; set => balance = value; }
        public string AccountNumber { get => accountNumber; set => accountNumber = value; }
        //METHOD FOR WITHDRAW MONEY 
        abstract public double Withdraw(double withdrawAmount);

        //METHOD FOR DEPOSIT MONEY
        abstract public double Deposit(double depositAmount);
        



    }

}
