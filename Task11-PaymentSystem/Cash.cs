﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Cash
    {
        private double value;
        public Cash()
        {
            
        }
        public Cash(double value)
        {
            this.value = value;
        }
        public double Value { get => value;  }
    }
    
}
