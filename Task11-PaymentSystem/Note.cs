﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Note : Cash
    {
        public Note(double value) : base(value)
        {
            if (value != 50 && value != 100 && value != 200 && value != 500 && value != 1000)
            {
                Console.WriteLine("Accepted notes are 50,200,500,1000");
            }

        }

        public override string ToString()
        {
            Console.WriteLine($"this is {Value} note in NOK  ");
            return $"this is {Value}  note in NOK  ";
        }
    }
}
