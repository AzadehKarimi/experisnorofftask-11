﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class CreditCard : Card
    {
        public override double Withdraw(double withdrawAmount)
        {
            if (Balance - withdrawAmount<0)
            {
                Console.WriteLine($"Your balance  in credit card is under limitted { Balance = Balance - withdrawAmount}");
                return Balance ;

            }
            else
            {
                Console.WriteLine($"Your balance  in credit card is  { Balance = Balance - withdrawAmount}");
                return Balance;

            }
           
        }

        public override double Deposit(double depositAmount)
        {
            Console.WriteLine($"Your balance  in credit card is { Balance = Balance + depositAmount}");
            return Balance ;
            
           
        }
    }
}
