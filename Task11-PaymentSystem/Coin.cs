﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Coin : Cash
    {
        public Coin(double value) : base(value)
        {
            if(value!=1 && value != 5 && value != 10 && value != 20)
            {
                Console.WriteLine("Accepted coins is 1,5,10,20");
            }
            
        }

        public override string ToString()
        {
            Console.WriteLine($"this is {Value} coins in NOK  ");
            return $"this is {Value} coins in NOK  ";
        }
        
    }
}
